const notas = [7.7, 6.5, 4.4, 3.3, 2.2, 1.1];

//sem callback

const notasBaixas1 = []
for (let i in notas) {
    if (notas[i] < 7) {
        notasBaixas1.push(notas[i]);
    }
}

console.log(notasBaixas1);


// Com Callback

const notasBaixas2 = notas.filter(function (nota) {
    return nota < 7;
})

console.log(notasBaixas2)

//

const notasBaixas3 = notas.filter(nota => nota < 7);
console.log(notasBaixas3)

const notasBaixas4 = notas.filter(nota => nota > 5);
console.log(notasBaixas4);