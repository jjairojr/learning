//Dia
let today = new Date();
let day = today.getDay();
const daylist = ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"];
console.log("Hoje é: " + daylist[day] + ".");

//Horas
let hour = today.getHours();
let minutes = today.getMinutes();
let seconds = today.getSeconds();
let season = (hour >= 12) ? " PM" : " AM";
hour = (hour >= 12) ? hour - 12 : hour;
if (hour === 0 && season === ' PM ') {
    if (minutes === 0 && seconds === 0) {
        hour = 12;
        season = " Noon";
    }
    else {
        hour = 12;
        season = " PM";
    }
}

if (hour === 0 && season === ' AM ') {
    if (minutes === 0 && seconds === 0) {
        hour = 12;
        season = " Midnight";
    }
    else {
        hour = 12;
        season = " AM"
    }
}
console.log(`São ${hour}${season} : ${minutes} : ${seconds}`);
