/*function pessoa ()
{
    this.idade = 0;

    setInterval(() => {
        this.idade++
        console.log(this.idade);
    }, 1000);
}

new pessoa; */


let compara = function (param)
{
    console.log(this === param)
}

compara(global);
/* */
 const obj = {}
 compara = compara.bind(obj)
 compara(global)

 /* arrow */

 let comparaArrow = param => console.log(this === param)
 comparaArrow(global);
 comparaArrow(module.exports)

 /* tentar modificar o this usando .bind na função arrow*/

 comparaArrow = comparaArrow.bind(obj);
 comparaArrow(obj) //a Arrow function se sobressai//
 


